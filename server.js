/**
 * Server.js
 * @author : GalileoGuzmán | https://twitter.com/galileoguzman
 * @Created on: 28 July, 2015
 */

/* Librerias necesarias para la aplicación */
var bodyParser  = require('body-parser');
var express     = require('express');
var app         = express();
var http        = require('http').Server(app);
var io          = require('socket.io')(http);
var MongoClient = require('mongodb').MongoClient;
var userDAO     = require('./dao/UserDAO').UserDAO;
var messageDAO = require('./dao/MessageDAO').MessageDAO;

/*
* static files
* with xpress in a folder
*/

app.use('/static', express.static('public'));

// Para acceder a los parametros de las peticiones POST
app.use(bodyParser());

/* Mongodb config */
var mdbconf = {
  host: 'localhost',
  port: '27017',
  db: 'chatSS'
};

/* Get a mongodb connection and start application */
MongoClient.connect('mongodb://'+mdbconf.host+':'+mdbconf.port+'/'+mdbconf.db, function (err, db) {
  
  if (err) return new Error('Connection to mongodb unsuccessful');
  
  var usersDAO = new userDAO(db); // Initialize userDAO
  var messagesDAO = new messageDAO(db);
  var onlineUsers = [];
  
  
/** *** *** ***
 *  Configuramos el sistema de ruteo para las peticiones web:
 */
  
  app.get('/signup', function (req, res) {
    res.sendFile( __dirname + '/views/signup.html');
  });
  
  app.post('/signup', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var email    = req.body.email;
    
    console.log("POST FORM - Los datos son usr : " + username + " psw : " + password +" email : " + email);

    usersDAO.addUser(username, password, email, function (err, user) {
      if (err) {
        //Se cambiaron las funciones en la condición
        user.password = null;
        res.send({ 'error': false, 'user': user });
      }
      else {
        res.send({ 'error': true, 'err': err});
      }
    });
  });

  app.post('/login', function (req, res) {

    console.log("peticion de inicio de sesión");

    var username = req.body.username;
    var password = req.body.password;
    
    usersDAO.validateLogin(username, password, function (err, user) {
      if (err) {
        console.log(err);
        res.send({'error': true, 'err': err});
      }
      else {
        console.log("Usuario autenticado");
        user.password = null;
        res.send({ 'error': false, 'user': user});
      }
    });
  });
  
  /** css and js request */
  app.get('/css/estilo.css', function (req, res) {
    res.sendFile(__dirname + '/views/css/estilo.css');
  });

  app.get('/css/foundation.min.css', function (req, res) {
    res.sendFile(__dirname + '/views/css/foundation.min.css');
  });

  app.get('/css/normalize.css', function (req, res) {
    res.sendFile(__dirname + '/views/css/normalize.css');
  });
  
  app.get('/js/foundation.min.js', function (req, res) {
    res.sendFile(__dirname + '/views/js/foundation.min.js');
  });
  app.get('/js/moment-with-locales.js', function (req, res) {
    res.sendFile(__dirname + '/views/js/moment-with-locales.js');
  });
  app.get('/js/chat.js', function (req, res) {
    res.sendFile(__dirname + '/views/js/chat.js');
  });
  /** *** *** */
  
  app.get('/', function(req, res) {
    res.sendFile( __dirname + '/views/chat.html');
  });


  /** *** *** ***
   *  Configuramos Socket.IO para estar a la escucha de
   *  nuevas conexiones. 
   */
  io.on('connection', function(socket) {
    
    console.log('New user connected');
    
    /**
     * Cada nuevo cliente solicita con este evento la lista
     * de usuarios conectados en el momento.
     */
    socket.on('all online users', function () {
      socket.emit('all online users', onlineUsers);
    });
    
    /**
     * Cada nuevo socket debera estar a la escucha
     * del evento 'chat message', el cual se activa
     * cada vez que un usuario envia un mensaje.
     * 
     * @param  msg : Los datos enviados desde el cliente a 
     *               través del socket.
     */
    socket.on('chat message', function(msg) {

      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();

      today = dd+'-'+mm+'-'+yyyy;

      messagesDAO.addMessage(msg.username, today, msg.message, function (err, nmsg) {
        console.log(today);
        io.emit('chat message', msg);
      });
    });
    
    /**
     * Mostramos en consola cada vez que un usuario
     * se desconecte del sistema.
     */
    socket.on('disconnect', function() {
      onlineUsers.splice(onlineUsers.indexOf(socket.user), 1);
      io.emit('remove user', socket.user);
      console.log('User disconnected');
    });

    /**
     * Cada nuevo cliente solicita mediante este evento
     * los ultimos mensajes registrados en el historial
     */
    socket.on('latest messages', function () {
      messagesDAO.getLatest(50, function (err, messages) {
        if (err) console.log('Error getting messages from history');
        socket.emit('latest messages', messages);
      });
    });
    
    /**
     * Cuando un cliente se conecta, emite este evento
     * para informar al resto de usuarios que se ha conectado.
     * @param  {[type]} nuser El nuevo usuarios
     */
    socket.on('new user', function (nuser) {
      socket.user = nuser;
      onlineUsers.push(nuser);
      io.emit('new user', nuser);
    });
    
  });
  
  /**
   * Iniciamos la aplicación en el puerto 3000
   */
  http.listen(8080, function() {
    console.log('listening on *:8080');
  });
});